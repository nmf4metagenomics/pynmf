# PyNMF

## Description
PyNMF allows the resolution of Non-Negative Matrix Factorization (NMF) on metagenomics data with additional metabolic constraints (see [Raguideau et al., 2016](https://doi.org/10.1371/journal.pcbi.1005252) for details on the method).

This repository contains:
- the NMF computation chore script.
- a jupyter notebook examplifying the API
- a testing database used in [Labarthe et al.](https://hal.inrae.fr/hal-03918193)

## Installation

A dedicated virtual environment is installed, including required dependencies

**Install virtualenv (optional, if virtualenv is not present on your computer)**

```
pip install virtualenv
```

**Create a new virtual envirionment**

```
virtualenv PyNMF
source PyNMF/bin/activate
pip install -r requirements.txt
python -m ipykernel install --user --name=PyNMF
deactivate
```

## Usage

A jupyter notebook details usage of the API to compute a NMF on a metagenomic data set, with given metabolic constraints.

**Launch the jupyter notebook**

```
source PyNMF/bin/activate
jupyter notebook
```

Select the file 'NMF_dataset.ipynb'


## Authors

Code: Simon Labarthe
Development of the method: Sebastien Raguideau, Sandra Plancade, Marion Leclerc, Béatrice Laroche

## Citation
If you use this script, please cite [Labarthe et al.](https://hal.inrae.fr/hal-03918193) and [Raguideau et al.](https://doi.org/10.1371/journal.pcbi.1005252)


## License
This script has been developed under the GNU GPL v3 license. The OSQP package is licensed under the Apache v2 license. 


