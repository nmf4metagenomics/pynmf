#!/usr/bin/python3
# -*- coding: latin-1 -*-
import numpy as np
import osqp
import scipy as sp
import scipy.sparse as scp
import scipy.linalg as scl
import pandas as pd
from pathlib import Path
import time
import os
from sklearn.model_selection import KFold


##### This file contains two classes: DataSet and NMF. The classes DataSet wraps the data set, preform precomputing tasks (such as normalization). The class NMF performs the two non-linear least-square regression involved in the block-descent algorithm for NMF computation.



class DataSet(object):
    """
    Class that manages data sets.
    """

    def __init__(self):

        self.Normalized=None

        self.Data=None
        self.Norm_Data=None



    def GetData(self,X0):
        """
        Getting Data and test
        
        input: 
            - X0 : data (numpy array or pandas dataframe)          
        """

        if isinstance(X0, pd.DataFrame):
            self.columns = X0.columns
            self.index=X0.index
            self.Data = X0.to_numpy()
        elif isinstance(X0,np.ndarray):
            self.Data = X0.copy()
        else:
            raise NameError('Input data must be a numpy array or a pandas dataframe')
        self.Normalized=False
       
        self.Norm_Data=np.linalg.norm(self.Data,axis=0)


    def ColNorm(self):
        """
        Data Normalization
        
        Normalize the data by column and turn the normalization flag to True
        """

        #Column normalization
        eps=1e-15
        inv_norm_data=scp.diags(1.0/(self.Norm_Data+eps))
        self.Data = inv_norm_data.T.dot(self.Data.T) # transpose to have sparse matrix multiplication => faster
        self.Data=self.Data.T
        self.Normalized=True

    def ColUnNorm(self):
        """
        Data de-normalization
        
        Normalize the data by column back to the original data scale, and turn the normalization flag back to False
        """
    
        #Column de-normalization
        norm_data=scp.diags(self.Norm_Data)
        self.Data = norm_data.T.dot(self.Data.T) # transpose to have sparse matrix multiplication => faster
        self.Data=self.Data.T
        self.Normalized=False




class NMF(object):
    '''
    NMF toolbox

    This class manages all the functions for computing a constrained NMF with L1-L2 regularization (see Raguideau et al., 2016, https://doi.org/10.1371/journal.pcbi.1005252)
    Namely, we seek to solve, for X0 \in \R^{N_obs,N_f},
        W,H = argmin_{W \in \R^{N_obs,K}, H \in \R{K,N_f}; W,H>0 ; F.H>0} \| (X0 - WH)D^{-1}\|^2_2 + alpha (\|W\|^2_2 + \|1_K.T HD^{-1}\|_2^2 ) 
    where N_obs is the number of observations, N_f the number of features, 1_K is the all-ones array of dimension (K,1) where K is a (dimensional) hyper-parameter, and alpha is a regularization hyper-parameter. D is a scaling diagonal matrix: the element D_{i,i} is the L2 norm of the i-th column of X0.
        
    The NMF is solved with an alternate optimization algorithm, the positive regression being solved with the OSQP toolbox: https://osqp.org, 
        @article{osqp,
          author = {Stellato, B. and Banjac, G. and Goulart, P. and Bemporad, A. and Boyd, S.},
          title = {{OSQP}: An Operator Splitting Solver for Quadratic Programs},
          journal = {ArXiv e-prints},
          year = {2017},
          month = nov,
          adsnote = {Provided by the SAO/NASA Astrophysics Data System},
          adsurl = {http://adsabs.harvard.edu/abs/2017arXiv171108013S},
          archiveprefix = {arXiv},
          eprint = {1711.08013},
          keywords = {Mathematics - Optimization and Control},
          primaryclass = {math.OC},
        }        
    '''
    def __init__( 
                    self,
                    NMFParameters_dict=None,
                    default_param={"Max_iter":5e5,'Threshold':1e-4, 'output_path':'NMF_results/','Initialization_Number':1,'Log_Output':'NMF.log'}
                    ):
        ''' Class instanciation.

        Input :
            - NMFParameters_dict: NMF parameter to be given to the class. In particular, it must provide the following parameters:
                * Alpha value (positive float): value of the regularization parameter
                * K (int): dimension of the small dimensional space where W and H must be searched
                * Threshold (float, default = 1e-4): precision threshold tuning the stopping criteria of the NMF
                * Max_iter (int, default = 5e5) : maximal number of iteration cycle
                * F (numpy array or pandas dataframe or None) : metabolic constraint coded as linear constraints on H (see Raguideau et al., 2016, https://doi.org/10.1371/journal.pcbi.1005252)
                * Initialization_Number : number of repetitions of NMF
        '''
        if NMFParameters_dict!=None:
            self.param = NMFParameters_dict
            # default parameters
            for dfp in default_param.keys():
                if dfp not in self.param.keys():
                    self.param[dfp] = default_param[dfp]
            self.initialization_number=self.param['Initialization_Number']
             
            if isinstance(self.param['F'], pd.DataFrame):
                self.TildeF = self.param['F'].to_numpy()
            elif 'X0' in locals() and isinstance(X0,np.ndarray):
                self.TildeF = self.param['F'].copy()
            else:
                print('Warning, constraint matrix is None')
                self.TildeF = None

            self.Alpha = self.param['Alpha']

            self.K = self.param['K']
            self.output_path = self.param['output_path']
            
            Path(self.output_path).mkdir(parents=True, exist_ok=True)
            

            self.Iterations=0


            self.solver='qdldl'

        else:
            raise NameError('NMF parameter dictionnary not provided')




    def RandHW(self,X):
        """
        Creates random H and W matrices

        This functions creates random H and W matrices with the correct dimensions and normalized so that ||X||=||WH||
        """
        W=np.random.uniform(size=(X.shape[0],self.K))
        H=np.random.uniform(size=(self.K,X.shape[1]))
        normWH = np.linalg.norm(W.dot(H))
        normX=np.linalg.norm(X)

        W=W*np.sqrt(normX/normWH)
        H=H*np.sqrt(normX/normWH)

        return W,H



    def Optimize_H(self,X,W,F=None,alpha=1,tol=1e-6,verbose=False,max_iter=100000):
        """Wraps the quadratic programming problem corresponding to l2-l1 mixed-norm with the OSQP solver"""

        """ Given the matrix X (n*r), and the matrix W (n*k), we aim to solve the problem 
            minimize in R^{k*r} ||X - WH||_2^2 + alpha [[1.T H]]_2^2
            subject to  : H>=0 , F.H^T<=0, if F is present,    or      H>=0 if F is not present
        which is equivalent to solve
            minimize 1/2 * 2 * y^t y + 1/2 *2 * alpha t^t t 
            subject to 
                y = (X - WH)
                t=  1.T H
                H>=0
                F.H^T<=0 (if F is present)

        which is equivalent to

            minimize 1/2 z^t P z + q z
            subject to 
                l <= A z <= u
        with     z = (y,t,H) in R^(n x r) x R^r x R^(k x r), 
            P=    (2 * Id_{n x r},            0_{n x r,r},        0_{n x r, k x r};
                0_{r,n x r},            2 * alpha *Id_{r},        0_{r,k x r} ;
                0_{k x r,n x r},    0_{k x r,r},    0_{k x r} )
            q=(0_{1,n x r},    0_{1,r},    0_{1,k x r})
            l = (X.flatten('F') , 0_{1,r} , 0_{1,k x r}, (OPTIONAL: - np.inf * 1_{1,F.shape[0] x k}))
            u = (X.flatten('F') , 0_{1,r} , np.inf * 1_{1,k x r}, (OPTIONAL:0_{1,F.shape[0] x k}) )
            A = (Id_{n x r},        0_{n x r,r},        kron(I_r,2*W);
                (0_{r, n x r},        I_r,            -kron(I_r,1_{1,k} ;
                (0_{k x r, n x r},    0_{k x r, r},         Id_{k x r})    
                (OPTIONAL  0_{k x F.shape[0], n x r},    0_{k x F.shape[0], r},         kron(F,I_k))

            NB : X.flatten('F') is actually the vector made with the concatenation of the columns of X. flatten is the corresponding numpy function, 'F' being the Fortran convention for array indexing. kron refears to the scipy.sparse function kron, which stands for the kronecker product 

        We solve this problem with OSQP : https://osqp.org, 
        @article{osqp,
          author = {Stellato, B. and Banjac, G. and Goulart, P. and Bemporad, A. and Boyd, S.},
          title = {{OSQP}: An Operator Splitting Solver for Quadratic Programs},
          journal = {ArXiv e-prints},
          year = {2017},
          month = nov,
          adsnote = {Provided by the SAO/NASA Astrophysics Data System},
          adsurl = {http://adsabs.harvard.edu/abs/2017arXiv171108013S},
          archiveprefix = {arXiv},
          eprint = {1711.08013},
          keywords = {Mathematics - Optimization and Control},
          primaryclass = {math.OC},
        }"""


        n = W.shape[0]
        if X.shape[0] != n:
            raise  NameError('Non consistent shapes for X and W')

        r = X.shape[1]
        k = W.shape[1]

        I_n_r = scp.eye(n*r, format='csc')
        I_k_r = scp.eye(k*r, format='csc')
        I_r = scp.eye(r, format='csc')
        I_k = scp.eye(k, format='csc')

        #OPSQ data
        P = scp.block_diag((2*I_n_r, 2 *alpha* I_r, scp.csc_matrix((k * r, k * r))), format='csc')
        q = np.zeros(n*r + r + k * r)
        if F is None:
            A = scp.vstack([scp.hstack([I_n_r,  scp.csc_matrix((n * r, r))   , scp.kron(I_r,W, format='csc')]),
                   scp.hstack([scp.csc_matrix((r, n * r )), I_r, -scp.kron(I_r,np.ones((1,k)) ,format='csc')]), 
                   scp.hstack([scp.csc_matrix((k*r, n * r )), scp.csc_matrix((k*r, r )), I_k_r])]).tocsc()
            l = np.hstack([X.flatten(order='F'), np.zeros((r,)), np.zeros((k*r,))])
            u = np.hstack([X.flatten(order='F'), np.zeros((r,)), np.inf * np.ones((k*r,))])
        else:
            A = scp.vstack([scp.hstack([I_n_r,  scp.csc_matrix((n * r, r))   , scp.kron(I_r,W, format='csc')]),
                   scp.hstack([scp.csc_matrix((r, n * r )), I_r, -scp.kron(I_r,np.ones((1,k)) ,format='csc')]), 
                   scp.hstack([scp.csc_matrix((k*r, n * r )), scp.csc_matrix((k*r, r )), I_k_r]),
                   scp.hstack([scp.csc_matrix((k*F.shape[0], n * r )), scp.csc_matrix((k*F.shape[0], r )), scp.kron(F,I_k, format='csc')])]).tocsc()
            l = np.hstack([X.flatten(order='F'), np.zeros((r,)), np.zeros((k*r,)),-np.inf * np.ones((k*F.shape[0],))])
            u = np.hstack([X.flatten(order='F'), np.zeros((r,)), np.inf * np.ones((k*r,)),np.zeros((k*F.shape[0],))])

        # Create an OSQP object
        prob = osqp.OSQP()


        # Setup workspace
        prob.setup(P=P, q=q, A=A, l=l, u=u, eps_prim_inf=tol/10.0,eps_dual_inf=tol/10.0,eps_abs=tol,eps_rel=tol,max_iter=max_iter,verbose=verbose,linsys_solver=self.solver)
        # Problem Solve
        res_prob = prob.solve()

        # Recover H
        h = res_prob.x[n*r + r : ]
        H = h.reshape((k,r),order='F')

        return H




    def Optimize_W(self,X,H,alpha=1,tol=1e-6,verbose=False,max_iter=100000):
        """Wraps the quadratic programming problem corresponding to l2 mixed-norm with the OSQP solver"""

        """ Given the matrix X (n*r), and the matrix H (k*r), we aim to solve the problem 
            minimize in R^{n*k} ||X^t - H^tW^t||_2^2 + alpha [[W^t]]_2^2
            subject to  : W>=0 
        which is equivalent to solve
            minimize 1/2 2 * y^t y + 1/2 *2 * alpha W W^t
            subject to 
                y = (X^t - H^tW^t)
                W>=0

        which is equivalent to

            minimize 1/2 z^t P z + q z
            subject to 
                l <= A z <= u
        with     z = (y,W) in R^(n x r) x R^(n x k), 
            P=    (2 * Id_{n x r},            0_{n x r,k x n};
                0_{k x n,n x r},            2 * alpha *Id_{k x n})
            q=(0_{1,n x r},    0_{1,k x n})
            l = (X.flatten('C') , 0_{1,k x n})
            u = (X.flatten('C') , np.inf * 1_{1,k x n})
            A = (Id_{n x r},        kron(I_n,H^t);
                (0_{ k x n, n x r},        I_k_n)


            NB : X.flatten('C') = X^T.flatten('F') is actually the vector made with the concatenation of the rows of X. flatten is the corresponding numpy function, 'C' being the C convention and 'F' being the Fortran convention for array indexing. kron refears to the scipy.sparse function kron, which stands for the kronecker product 

        We solve this problem with OSQP : https://osqp.org, 
        @article{osqp,
          author = {Stellato, B. and Banjac, G. and Goulart, P. and Bemporad, A. and Boyd, S.},
          title = {{OSQP}: An Operator Splitting Solver for Quadratic Programs},
          journal = {ArXiv e-prints},
          year = {2017},
          month = nov,
          adsnote = {Provided by the SAO/NASA Astrophysics Data System},
          adsurl = {http://adsabs.harvard.edu/abs/2017arXiv171108013S},
          archiveprefix = {arXiv},
          eprint = {1711.08013},
          keywords = {Mathematics - Optimization and Control},
          primaryclass = {math.OC},
        }"""
        n = X.shape[0]
        r = X.shape[1]
        if H.shape[1] != r:
            raise  NameError('Non consistent shapes for X and H')

        k = H.shape[0]

        I_n_r = scp.eye(n*r, format='csc')
        I_k_n = scp.eye(k*n, format='csc')
        I_n = scp.eye(n, format='csc')

        #OPSQ data
        P = scp.block_diag((2 * I_n_r, 2 *alpha* I_k_n), format='csc')
        q = np.zeros(n*r +  k * n)
        A = scp.vstack([scp.hstack([I_n_r,   scp.kron(I_n,H.T, format='csc')]),
               scp.hstack([scp.csc_matrix((k*n, n * r )), I_k_n])]).tocsc()
        l = np.hstack([X.flatten(order='C'), np.zeros((k*n,))])
        u = np.hstack([X.flatten(order='C'), np.inf * np.ones((k*n,))])


        # Create an OSQP object
        prob = osqp.OSQP()


        # Setup workspace
        prob.setup(P=P, q=q, A=A, l=l, u=u,eps_prim_inf=tol/10.0,eps_dual_inf=tol/10.0,eps_abs=tol,eps_rel=tol,max_iter=max_iter,verbose=verbose,linsys_solver=self.solver)
        # Problem Solve
        res_prob = prob.solve()

        # Recover H
        w = res_prob.x[n*r : ]
        W = w.reshape((n,k),order='C') #order='C' because we actually infer W^T

        return W


    def NMF_BCD(self,X,W_init,H_init,F=None,tol=1.0e-6,verbose=False):
        """ Perform a constrained (or unconstrained, depending on the presence of F) block coordinate descent to solve the NMF

        Solves the NMF problem by alternatively solving positive non-linear regression by infering W for fixed H or infering H for fixed W. Stops when successive inference (after 100 iterations) of W and H present small relative error. N.B. : the option F allows to pass to the function a constraint matrix. The function checks if the constraint matrix is embedded or not in the argument and select the corresponding inference problem for H
        """

        with open(self.output_path+self.param['Log_Output'],'a') as f:
            f.write(15*"*"+'\n')
            f.write('Initiating NMF'+'\n')
            for p in self.param.keys():
                f.write(p+':'+str(self.param[p])+'\n')    
            f.write(15*"*"+'\n')
        alpha=self.Alpha
        threshold=self.param['Threshold']

        
        Iteration=self.Iterations
        Max_iter=self.param['Max_iter']
        eps=1e-14
        
        print(W_init.shape, H_init.shape)
        Time_W=0
        Time_H=0
        convergence = False
        Iteration=1
        WH_ref=W_init.dot(H_init)
        W=W_init.copy()
        H=H_init.copy()
        while (convergence==False):

            #/****** Optimization on H ******/
            begin=time.time()
            H=self.Optimize_H(X, W,F=F,alpha=alpha,tol=threshold)
            Time_H+=time.time()-begin
            if verbose:
                print('Time Inference H',time.time()-begin)
            #/****** Optimization on W *****
            begin=time.time()
            W=self.Optimize_W(X,H,alpha=alpha,tol=threshold)
            Time_W+=time.time()-begin
            if verbose:
                print('Time Inference W',time.time()-begin)
            #/******** Convergence check ********/
            if (Iteration%10==0):
                print(Iteration,'Time W',Time_W,'Time H',Time_H,"Total",Time_W+Time_H)
                WH_curr=W.dot(H)
                err=np.linalg.norm(WH_curr-WH_ref)
                err_norm=np.linalg.norm(WH_curr+WH_ref)/2.0
                err_rel=err/(err_norm+eps)
                if err_rel<threshold or Iteration>=Max_iter:
                    convergence=True
                else:
                    WH_ref=WH_curr
            Iteration+=1
            print('Iteration '+str(Iteration))
            with open(self.output_path+self.param['Log_Output'],'a') as f:
                f.write('Iteration '+str(Iteration)+'\n')

        return W,H








    def Simple(self,X0,verbose=False,tol=1.0e-6):
        """
        Perform a constrained NMF

        From a data set, performs a constrained NMF, and deals with renormalization of W and H, and ordering of profiles. Performs 'Initialization_Number' repetitions with different initial guesses, and take the best.
        Input:
            - X0 : data to be factorized
        """
        self.X0=DataSet()
        self.X0.GetData(X0)
        self.X0.ColNorm() #column normalization of dataset

        self.H=np.zeros((self.K,self.X0.Data.shape[1]))
        self.W=np.zeros((self.X0.Data.shape[0],self.K))

        
        #try different initializations for W1 H1 for NMF of X1
        begin=time.time()
        Best_err=np.inf
        for init in np.arange(self.param['Initialization_Number']):
            with open(self.output_path+self.param['Log_Output'],'a') as f:
                f.write(50*'='+'\n')
                f.write(20*"*"+'\n')
                f.write('NMF replicate '+str(init+1)+'/'+str(self.param['Initialization_Number'])+'\n')
                f.write(20*"*"+'\n')
            W_temp,H_temp=self.RandHW(self.X0.Data)
            W_temp,H_temp=self.NMF_BCD(self.X0.Data,W_temp,H_temp,F=self.TildeF,tol=tol,verbose=verbose)
            curr_err=np.linalg.norm(self.X0.Data-W_temp.dot(H_temp))            
            if init==0 or curr_err<Best_err:
                W=W_temp
                H=H_temp
                Best_err=curr_err
        Best_err=Best_err/np.linalg.norm(self.X0.Data)
        Time=time.time()-begin

        #Compute error
        Res = self.X0.Data-W.dot(H)
        ErrN = np.linalg.norm(Res)/np.linalg.norm(self.X0.Data)
        Norm = scp.diags(self.X0.Norm_Data)
        Err = np.linalg.norm(Norm.dot(Res.T))/np.linalg.norm(Norm.dot(self.X0.Data.T))


        if self.X0.Normalized:
            H=Norm.dot(H.T)
            H=H.T


        #############################Test
        if verbose:
            G=self.TildeF.dot(H.T)
            print(G)
            I = G>tol
            print(G[I])
            print(np.sum(np.sum(G[I])))
            print('number of constraints not found :',np.sum(np.sum(I)))


        ##############################################
        #######L1 normalization of W and H by profiles
        #H1 norm of H and normalization of W and H 
        HL1norm=np.linalg.norm(H,axis=1,ord=1)
        Norm = scp.diags(1.0/HL1norm)
        H=Norm.dot(H)
        Norm = scp.diags(HL1norm)
        W=Norm.dot(W.T)
        W=W.T

        #Sort W and H according to L1 norm of W columns
        WL1norm=np.linalg.norm(W,axis=0,ord=1)
        idxPermSort=np.argsort(-WL1norm) #sign minus to have a descending order sorting of the vector
        W = W[:,idxPermSort]
        H = H[idxPermSort,:]
        self.W = W
        self.H = H



        ToPrint="Alpha="+str(self.Alpha)+"\nK="+str(self.K)+"\nInitialization number="+str(self.param['Initialization_Number'])+"\nErrN="+str(ErrN)+"\n"+"Err="+str(Err)+"\nTemps="+str(Time)+"\nNMF Iterations="+str(self.Iterations)

        print(ToPrint)
        with open(self.output_path+self.param['Log_Output'],'a') as f:
            f.write(ToPrint+'\n')
            
        File_output=self.output_path+'W_K='+str(self.K)+"Alpha="+str(self.Alpha)+'.txt'
        if hasattr(self.X0, 'index'):
            tmp_W = pd.DataFrame(self.W,index=self.X0.index,columns=['Profile'+str(i+1) for i in range(W.shape[1])])
            tmp_W.to_csv(File_output,sep='\t')
        else:
            tmp_W=self.W
            np.savetxt(File_output,tmp_W)

        File_output=self.output_path+'H_K='+str(self.K)+"Alpha="+str(self.Alpha)+'.txt'
        if hasattr(self.X0, 'columns'):
            tmp_H = pd.DataFrame(self.H,index=['Profile'+str(i+1) for i in range(W.shape[1])],columns=self.X0.columns)
            tmp_H.to_csv(File_output,sep='\t')
        else:
            tmp_H=self.H    
            np.savetxt(File_output,tmp_H)
        return tmp_W, tmp_H
